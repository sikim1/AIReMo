# 현재 운영 중인 모델 모니터링 페이지
from dash import Dash, html, dcc, Input, Output
import dash_bootstrap_components as dbc

import pandas as pd
import plotly.express as px
import plotly.graph_objs as go

# external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
# app = Dash(__name__, external_stylesheets=external_stylesheets)
app = Dash(external_stylesheets=[dbc.themes.BOOTSTRAP], suppress_callback_exceptions=True)

tab1_content = dbc.Card(
    dbc.CardBody(
        [
            html.H4("추론 결과 집계", className="card-title", style={"margin": "auto", 'textAlign': 'center', "fontSize": "15px"}),
            html.P("Last", style={"fontSize": "7px"}),
            dbc.RadioItems(
                id="summary_radios",
                class_name="btn-group",
                input_class_name="btn-check",
                label_class_name="btn btn-outline-primary",
                label_checked_class_name="active",
                options=[
                    {"label": "1 Day", "value": 1},
                    {"label": "7 days", "value": 2},
                    {"label": "15 days", "value": 3},
                    {"label": "30 days", "value": 4},
                    {"label": "60 days", "value": 5},
                ],
                value=1,
                input_style={"width": "100%", "height": 2, "margin-left": "0rem", "margin-right": "0rem", "padding": "0rem 0rem"},
                label_style={"fontSize": "3px"}
            ),
            # donut graph
            # table
        ]
    ),
    className="mt-3",
    style={"width": "30rem"},
)

tab2_content = dbc.Card(
    dbc.CardBody(
        [
            html.H4("예측 건수", className="card-title", style={"margin": "auto", 'textAlign': 'center', "fontSize": "15px"}),
            dbc.Row(
                [
                    dbc.Col(
                        html.P("Last", style={"fontSize": "7px"}),
                        style={"width": "5%", "margin": "auto"}
                    ),
                    dbc.Col(
                        [
                            dbc.RadioItems(
                                id="cnt_radios",
                                className="btn-group",
                                inputClassName="btn-check",
                                labelClassName="btn btn-outline-primary",
                                labelCheckedClassName="active",
                                inline=True,
                                options=[
                                    {"label": "7 days", "value": 1},
                                    {"label": "15 days", "value": 2},
                                    {"label": "30 days", "value": 3},
                                    {"label": "60 days", "value": 4},
                                ],
                                value=1,
                                input_style={"width": "100%", "height": 1.5, "margin-left": "0rem", "margin-right": "0rem", "padding": "0rem 0rem"},
                                label_style={"font-size": "0.5px"}
                            ),
                        ],
                        className="radio-group",
                        style={"width": "90%", "display": "flex", }
                    ),
                    dbc.Col(
                        "달력",
                        style={"width": "5%"}
                    )
                ],                
            ),            
            # calendar
            # time series
        ]
    ),
    className="mt-3",
    style={"width": "30rem"},
)

tab3_content = dbc.Card(
    dbc.CardBody(
        [
            html.H4("PSI", className="card-title", style={"margin": "auto", 'textAlign': 'center', "fontSize": "15px"}),
            html.P("Last", style={"fontSize": "7px"}),
            dbc.RadioItems(
                id="psi_radios",
                className="btn-group",
                inputClassName="btn-check",
                labelClassName="btn btn-outline-primary",
                labelCheckedClassName="active",
                options=[
                    {"label": "7 days", "value": 1},
                    {"label": "15 days", "value": 2},
                    {"label": "30 days", "value": 3},
                    {"label": "60 days", "value": 4},
                ],
                value=1,
                input_style={"width": 15},
                label_style={"fontSize": ".4rem"}
            ),
            # calendar
            # time series
        ]
    ),
    className="mt-3",
    style={"width": "30rem"},
)

SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "12rem",
    "padding": "1rem 1rem",
    "background-color": "#f8f9fa",
}

CONTENT_STYLE = {
    "width": "100rem",
    "height": "83rem",
    "margin-left": "12rem",
    "margin-right": "2rem",
   # "padding": "2rem 1rem",
}

sidebar = html.Div(
    [
        html.P("AIReMo", className="display-4", style={"font-size": "15px"}),
        html.Hr(),
        dbc.Nav(
            [
                dbc.NavLink("Home", href="/", active="exact"),
                dbc.NavLink("모델 관리", href="/models", active="exact"),
                dbc.NavLink("운영 모니터링", href="/monitor", active="exact"),
            ],
            vertical=True,
            pills=True,
        ),
    ],
    style=SIDEBAR_STYLE,
)

home_contents = html.Div(
    dbc.Row(
        [
            dbc.Col( # 운영 중인 모델 상태별 요약
                [
                    dbc.Row(
                        html.Div("운영 상태 donut chart"),
                        style={"height": "31rem", 
                               "padding": "1rem 1rem"},
                        justify="center"
                    ),
                    dbc.Row(
                        [
                            dbc.Card(
                                [
                                    dbc.CardBody(
                                        [
                                            html.H6(f"양호 | 7", className="card-title"),
                                            dbc.ListGroup(
                                                [
                                                    dbc.ListGroupItem("언더라이팅", active=True),
                                                    dbc.ListGroupItem("조사"),
                                                    dbc.ListGroupItem("고객 음성 삼당 분석"),
                                                    dbc.ListGroupItem("면책"),
                                                    dbc.ListGroupItem("계약유지율 예측"),
                                                    dbc.ListGroupItem("청약서류 OCR"),
                                                    dbc.ListGroupItem("차량 파손부위 탐지"),
                                                ], style={"fontSize": "5px"}
                                            )
                                        ],
                                        style={"padding": "0.1rem 0.1rem"}
                                    ),
                                ],
                                color="primary", outline=True,
                                className="mt-3",
                                style={"width": "10rem", "height": "15rem", "box-shadow": "5px 5px 10px lightgrey"},
                            ),
                            dbc.Card(
                                [
                                    dbc.CardBody(
                                        [
                                            html.H6(f"교체권장 | {3}", className="card-title"),
                                            dbc.ListGroup(
                                                [
                                                    dbc.ListGroupItem("AI 자동심사"),
                                                    dbc.ListGroupItem("청구서류 OCR"),
                                                    dbc.ListGroupItem("심사난이도"),
                                                ], style={"fontSize": "5px"}
                                            )
                                        ],
                                        style={"padding": "0.1rem 0.1rem"}
                                    ),
                                ],
                                color="warning", outline=True,
                                className="mt-3",
                                style={"margin-left": "1.5rem", "width": "10rem", "height": "15rem", "box-shadow": "5px 5px 10px lightgrey"},
                            ),
                            dbc.Card(
                                [
                                    dbc.CardBody(
                                        [
                                            html.H6(f"교체요망 | 2", className="card-title"),
                                            dbc.ListGroup(
                                                [
                                                    dbc.ListGroupItem("보험사기예측"),
                                                    dbc.ListGroupItem("보험상품추천"),
                                                ], style={"fontSize": "5px"}
                                            )
                                        ],
                                        style={"padding": "0.1rem 0.1rem"}
                                    ),
                                ],
                                color="danger", outline=True,
                                className="mt-3",
                                style={"margin-left": "1.5rem", "width": "10rem", "height": "15rem", "box-shadow": "5px 5px 10px lightgrey"},
                            ),
                            dbc.Card(
                                [
                                    dbc.CardBody(
                                        [
                                            html.H6(f"리모델링 | 1", className="card-title"),
                                            dbc.ListGroup(
                                                [
                                                    dbc.ListGroupItem("차량공임 자동분류"),
                                                ], style={"fontSize": "5px"}
                                            )
                                        ],
                                        style={"padding": "0.1rem 0.1rem"}
                                    ),
                                ],
                                color="dark", outline=True,
                                className="mt-3",
                                style={"margin-left": "1.5rem", "width": "10rem", "height": "15rem", "box-shadow": "5px 5px 10px lightgrey"},
                            ),
                        ],
                        style={"margin-left": "1.5rem"}
                    ),
                ],   
                style={"width": "70rem", "background-color": "#EAEDF5", "padding": "1rem 1rem"}
            ),
            dbc.Col( # 프로젝트 운영내용 요약
                dbc.Row(
                    [
                        dbc.Col([
                            dbc.Card(
                                [
                                    dbc.CardBody(
                                        [
                                            html.H5("언더라이팅", className="card-title"),
                                            html.P(
                                                "운영 효율화를 위해 보험 가입 고객을\n"
                                                "자동 심사하는 AI 모델 운영",
                                                className="card-text",
                                                style={"fontSize": "12px"}
                                            ),
                                        ]
                                    ),
                                ],
                                className="mt-3",
                                style={"width": "20rem"},
                            ),
                            dbc.Card(
                                [
                                    dbc.CardBody(
                                        [
                                            html.H5("KPI 기준", className="card-title"),
                                            html.P(
                                                "- 최근 1달간 고위험 등급 고객 비율 5%"
                                                "- 고위험 등급 타겟 비율 30% 유지",
                                                className="card-text",
                                                style={"fontSize": "12px"}
                                            ),
                                        ]
                                    ),
                                ],
                                className="mt-3",
                                style={"width": "20rem"},
                            ),
                            dbc.Card(
                                "운영 모델명 정보", 
                                className="mt-3",
                                style={"width": "18rem"},
                            ),
                            dbc.Card(
                                "일별 집계", 
                                className="mt-3",
                                style={"width": "18rem"},
                            ),
                        ]),
                        dbc.Col([
                            dbc.Card(
                                "최신 재학습 모델", 
                                className="mt-3",
                                style={"width": "18rem"},
                            ),
                            dbc.Card(
                                "공지사항", 
                                className="mt-3",
                                style={"width": "18rem"},
                            ),
                        ]),
                    ],
                    justify="center"
                ),
                style={"width": "30rem", "background-color": "#F7F7F7",
                       "padding": "1rem 2rem"}
            ),        
        ],
        className="g-0",
        style={"margin-left": "0rem", "margin-right": "0rem"},
    )
)

button_group = dbc.Col(
    [
        dbc.RadioItems(
            id="radios",
            className="btn-group",
            inputClassName="btn-check",
            labelClassName="btn btn-outline-primary",
            labelCheckedClassName="active",
            options=[
                {"label": "대시보드", "value": 1},
                {"label": "배포", "value": 2},
            ],
            value=1,
        ),
    ],
    # style={"margin-top": "0rem", "margin-bottom": "0rem"},
    className="radio-group",
)

data_info = dbc.Card( # body내에서 margin이 안먹혀서 다른거로 바꿔야할듯.... 그냥 col로 만들지 고민 중
    dbc.CardBody(
        [
            html.H6("성능 비교 데이터", className="card-title", 
                style={"margin": "auto", 'textAlign': 'center', "fontSize": "15px", "fontWeight": "bold", "margin-botton": "1px"}),
            dbc.Row(
                [
                    dbc.Col(
                        html.P("Last", style={"fontSize": "12px"}),
                        style={"width": "10%", "margin": "auto"}
                    ),
                    dbc.Col(
                        [
                            dbc.RadioItems(
                                id="data_range_radios",
                                class_name="btn-group",
                                input_class_name="btn-check",
                                label_class_name="btn btn-outline-primary",
                                label_checked_class_name="active",
                                inline=True,
                                options=[
                                    {"label": "7Days", "value": 1},
                                    {"label": "15Days", "value": 2},
                                    {"label": "30Days", "value": 3},
                                    {"label": "60Days", "value": 4},
                                ],
                                value=3,
                                input_style={"width": "100%", "height": "10px", "margin-left": "-5rem", "margin-right": "-2rem", "padding": "0rem 0rem"},
                                label_style={"fontSize": "0.5px", "margin-left": "0rem", "margin-right": "0rem", "padding": "0.2rem 0.5rem"}
                            ),
                        ],
                        className="radio-group",
                        style={"width": "85%", "display": "flex", }
                    ),
                    dbc.Col(
                        html.P("cal", style={"fontSize": "12px"}),
                        style={"width": "5%", "margin": "auto"}
                    )
                ],    
                style={"margin-top": "3px", "margin-botton": "3px"}            
            ),    
            dbc.Row(
                html.P("range따라 변경될 일자", style={"fontSize": "12px"}),
                style={"margin": "auto"},
                justify="center"
            ),
            dbc.Row(
                html.P("건수", style={"fontSize": "15px"}, id="data_cnt"),
                style={"width": "85%", "margin": "auto", "background-color": "#EAEDF5"}
            )
        ]
    ),
    className="mt-3",
    style={"width": "25rem", "height": "12rem", "margin-left": "10rem", "margin-right": "2px"},
)

slider_info = dbc.Card(
    dbc.CardBody(
        [
            html.H6("자동 심사율", className="card-title", style={"margin": "auto", 'textAlign': 'center', "fontSize": "15px", "fontWeight": "bold"}),
            dcc.Slider(0, 100, marks=None, value=30, id="thres-slider"),
            html.P(children="30", id="thres-result", style={"margin": "auto", 'textAlign': 'center', "fontSize": "30px", "fontWeight": "bold"}),
            dbc.Row(
                [
                    dbc.Button("Reset", id="reset_button", style={"width": "30%","margin-right": "2px"}, outline=True, color="dark", size="sm"),
                    dbc.Button("Save", id="save_button", style={"width": "30%", "margin-left": "2px"}, color="primary", className="me-1", size="sm"),
                ],
                justify="center"
            ),
            # calendar
            # time series
        ]
    ),
    className="mt-3",
    style={"width": "25rem", "height": "12rem", "margin-left": "2px", "margin-right": "2px"},
)

kpi_info = dbc.Card(
    dbc.CardBody(
        [
            html.H6("KPI 성능요약", className="card-title", style={"margin": "auto", 'textAlign': 'center', "fontSize": "15px", "fontWeight": "bold"}),
            dbc.Row(
                html.P("오류율 / 오지급 금액", style={"fontSize": "12px"}),
                style={"margin": "auto"},
                justify="center"
            ),
            dbc.Row(
                dcc.Markdown('''
                    * 자동심사율 **30%** 달성
                    * 오류율 **3%** 미만 달성
                ''', style={"fontSize": "12px"}),
                style={"width": "85%", "margin": "auto", "background-color": "#EAEDF5"}
            )
            # calendar
            # time series
        ]
    ),
    className="mt-3",
    style={"width": "25rem", "height": "12rem", "margin-left": "2px", "margin-right": "10rem"},
)

dashboard_contents = html.Div(
    [
        dbc.Row(
            [button_group],
            justify="center",
            style={"margin": "auto", "height": "3rem", "background-color": "#EAEDF5",},
        ),
        dbc.Row(
            [
                dbc.Col([data_info]), 
                dbc.Col([slider_info]),
                dbc.Col([kpi_info]),
            ],
            style={"margin": "auto", "height": "17rem", "background-color": "#EAEDF5",},
            justify="center"
        ),
        dbc.Row(
            [
                dbc.Col(
                    [
                        dbc.Card(
                            [
                                dbc.CardHeader("AUROC"),
                                dbc.CardBody(
                                    [
                                        html.P("0.8511", id="auroc", className="card-text"),
                                        html.P("현 모델 성능 - 운영 모델 성능", id="auroc_variance", className="card-text", style={"fontSize": "6px"}),
                                    ]
                                ),
                            ], color="secondary", outline=True, style={"margin-bottom": "3rem"}
                        ),
                        dbc.Card(
                            [
                                dbc.CardHeader("KS"),
                                dbc.CardBody(
                                    [
                                        html.P("0.5713", id="ks", className="card-text"),
                                        html.P("현 모델 성능 - 운영 모델 성능", id="ks_variance", className="card-text", style={"fontSize": "6px"}),
                                    ]
                                ),
                            ], color="secondary", outline=True, style={"margin-bottom": "3rem"}
                        ),
                        dbc.Card(
                            [
                                dbc.CardHeader("모집단 안정성 지수(PSI)"),
                                dbc.CardBody(
                                    [
                                        html.P("0.0002", id="psi", className="card-text"),
                                        html.P("현 모델 성능 - 운영 모델 성능", id="psi_variance", className="card-text", style={"fontSize": "6px"}),
                                    ]
                                ),
                            ], color="secondary", outline=True
                        ),
                    ], width=2
                ),
                dbc.Col(
                    [
                        html.P("혼동행렬", style={"fontSize": "15px"}),
                        dbc.Table(),
                        html.P("성능지표", style={"fontSize": "15px"}),
                        dbc.Table(),
                    ], 
                    lg=6,
                    style={"border": "0.5px solid lightgrey", "border-radius": "5px", "margin-right": "1rem"}
                ),
                dbc.Col(
                    [
    
                    ], 
                    lg=6,
                    style={"border": "0.5px solid lightgrey", "border-radius": "5px"}
                ),
            ], 
            justify="center",
            style={"margin": "auto", "height": "35rem", "background-color": "#FDFAFA", "padding": "2rem 1rem"},
        ),
    ]
)

monitor_contents = html.Div(
    [
        dbc.Row(
            "GOOD",
            justify="center",
            style={"margin": "auto", "height": "20rem", "background-color": "#EAEDF5",},
        ),
        dbc.Row(
            [
                dbc.Col(tab1_content),
                dbc.Col(tab2_content),
                dbc.Col(tab3_content),
            ], 
            justify="center",
            style={"margin": "auto", "height": "30rem", "background-color": "#F7F5F5",},
        ),        
    ],
    className="g-0",
)


content = html.Div(id="page-content", style=CONTENT_STYLE)

app.layout = html.Div([dcc.Location(id="url"), sidebar, content])

@app.callback(
    Output("thres-result", "children"), 
    Input("thres-slider", "value"))
def set_threshold(value):
    return str(value)

@app.callback(
    Output("page-content", "children"), 
    [Input("url", "pathname")])
def render_page_content(pathname):
    if pathname == "/":
        return home_contents
    elif pathname == "/models":
        return dashboard_contents
    elif pathname == "/monitor":
        return monitor_contents
    # If the user tries to reach a different page, return a 404 message
    return html.Div(
        [
            html.H1("404: Not found", className="text-danger"),
            html.Hr(),
            html.P(f"The pathname {pathname} was not recognised..."),
        ],
        className="p-3 bg-light rounded-3",
    )

if __name__ == '__main__':
    app.run_server(debug=True)