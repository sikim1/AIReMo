import warnings
warnings.filterwarnings('ignore')
# from skl2onnx import convert_sklearn, update_registered_converter,  to_onnx
import numpy as np
import pandas as pd

from sklearn.metrics import *
from sklearn import metrics

from tqdm import tqdm

def calculate_psi(expected, actual, buckettype='bins', buckets=10, axis=0):
    '''Calculate the PSI (population stability index) across all variables
    Args:
       expected: (numpy.ndarray) original values
       actual: (numpy.ndarray) new values, same size as expected
       buckettype: (str) type of strategy for creating buckets, bins splits into even splits, quantiles splits into quantile buckets
       buckets: (int) number of quantiles to use in bucketing variables
       axis: axis by which variables are defined, 0 for vertical, 1 for horizontal
    Returns:
       psi_values: ndarray of psi values for each variable
    ref:
       https://github.com/mwburke/population-stability-index/blob/master/psi.py
    '''

    def psi(expected_array, actual_array, buckets):
        '''Calculate the PSI for a single variable
        Args:
           expected_array: (numpy.ndarray) original values
           actual_array: (numpy.ndarray) new values, same size as expected
           buckets: number of percentile ranges to bucket the values into
        Returns:
           psi_value: calculated PSI value
        '''

        def scale_range(input, min, max):
            '''
            Args:
                input: breakpoints
                min: original values의 min
                max: original values의 max
            Returns:
                input: scaled breakpoints 
            '''
            input += -(np.min(input))
            input /= np.max(input) / (max - min)
            input += min
            return input

        breakpoints = np.arange(0, buckets + 1) / (buckets) * 100

        if buckettype == 'bins': # 균일한 간격으로 스케일
            breakpoints = scale_range(breakpoints, np.min(expected_array), np.max(expected_array))
        elif buckettype == 'quantiles': # 
            breakpoints = np.stack([np.percentile(expected_array, b) for b in breakpoints])

        expected_percents = np.histogram(expected_array, breakpoints)[0] / len(expected_array)
        actual_percents = np.histogram(actual_array, breakpoints)[0] / len(actual_array)

        def sub_psi(e_perc, a_perc):
            '''Calculate the actual PSI value from comparing the values.
               Update the actual value to a very small number if equal to zero
            '''
            if a_perc == 0:
                a_perc = 0.0001
            if e_perc == 0:
                e_perc = 0.0001

            value = (e_perc - a_perc) * np.log(e_perc / a_perc)
            return(value)

        psi_value = np.sum(sub_psi(expected_percents[i], actual_percents[i]) for i in range(0, len(expected_percents)))

        return(psi_value)

    if len(expected.shape) == 1:
        psi_values = np.empty(len(expected.shape))
    else:
        psi_values = np.empty(expected.shape[axis])

    for i in range(0, len(psi_values)):
        if len(psi_values) == 1:
            psi_values = psi(expected, actual, buckets)
        elif axis == 0:
            psi_values[i] = psi(expected[:,i], actual[:,i], buckets)
        elif axis == 1:
            psi_values[i] = psi(expected[i,:], actual[i,:], buckets)

    return(psi_values)

# add target drift
# add data drift 
# def drift():
#     return 0

def ks_stat(y_true, y_pred):
    """
    KS통계량을 구하는 함수
    Args:
        y_true: 실제 타겟값
        y_pred: 모델 예측값
    """
    y_true = np.array(y_true)
    y_pred = np.array(y_pred)
    
    scale = np.arange(1.00, 0.0, -0.001)
    total_pos = sum(y_true==1)
    total_neg = sum(y_true==0)
    
    ks_statistics = []
    
    for scale_ in scale:
        pos = len(y_true[(y_pred>=scale_) & (y_true==1)])/total_pos
        neg = len(y_true[(y_pred>=scale_) & (y_true==0)])/total_neg
        ks_statistics.append(pos-neg)
    return max(np.abs(ks_statistics))

def specific_ks_stat(y_true, y_pred, cut_off):
    """
    특정 cut_off 에서 KS통계량을 구하는 함수
    Args:
        y_true: 실제 타겟값
        y_pred: 모델 예측값
    """
    y_true = np.array(y_true)
    y_pred = np.array(y_pred)
    
    total_pos = y_true.sum() # 1의 합
    total_neg = abs((y_true-1).sum()) # 0의 합
     
    pos = len(y_true[(y_pred>=cut_off) & (y_true==1)])/total_pos
    neg = len(y_true[(y_pred>=cut_off) & (y_true==0)])/total_neg
    
    return pos-neg



def lift_score(y_true, y_pred, ratio=0.2):
    """
    상위 ratio 만큼의 LIFT를 구하는 함수
    Args:
        y_true: 실제 타겟값
        y_pred: 모델 예측값
    """
    df = np.vstack([y_true, y_pred]).T
    df = df[df[:,1].argsort()[::-1]]
    
    n_total = len(df)
    total_rate = df[:,0].sum()/n_total
    
    n_sample = round(n_total*ratio)
    sample_rate = df[:n_sample, 0].sum()/n_sample
    return sample_rate/total_rate


class ScoreManager(object):
    def __init__(self):
        self.metric_map = {
            'acc': accuracy_score,
            'auc': roc_auc_score,
            'recall': recall_score,
            'precision': precision_score,
            'f1': f1_score,
            'ks': ks_stat,
            'lift': lift_score
        }
        
    def __call__(self, y_true, y_pred, metric, **kwargs):
        try:
            metric_func = self.metric_map[metric]
            if 'threshold' in kwargs:
                threshold = kwargs['threshold']
                del kwargs['threshold']
            else:
                threshold = 0.5
            y_pred = self._get_y_pred(y_pred, metric, threshold)
            result = metric_func(y_true, y_pred, **kwargs)
            
            if result != result:
                print('y_true가 1개의 값으로만 이루어져 있어 0으로 반환합니다. 다른 score를 확인하세요.')
                setattr(self, metric, result=0)
                return 0
            else :
                setattr(self, metric, result)
            return result
        except:
            print('y_true 가 1개의 값으로만 이루어져 있어 0으로 반환합니다. 다른 Score를 확인하세요.')
            return 0
    
    def _get_y_pred(self, y_pred, metric, threshold):
        if metric in ['acc', 'recall', 'precision', 'f1']:
            y_pred = np.where(np.array(y_pred)>threshold, 1, 0)
            y_pred = pd.Series(y_pred)
        return y_pred

def threshold_optim(data, label, t_start = 0, t_end = 1):
    """
    최적 성능지표를 만족하는 threshold를 구하는 함수
    Args:
        data: (pd.DataFrame)
        label: (str)
    Returns:
        optim_th: (dict) optimal threshold 및 해당 thres에서의 성능지표 결과
    """
    t_range = np.arange(t_start, t_end, 0.01)
    threshold_matrix = pd.DataFrame(t_range, columns = ['Cut-off'])
    sc = ScoreManager()
    
    
    # 해당 라벨 데이터 EX) Y_0001
    label_data = data[label]
    prob_data = data[f'{label}_PROB']
    
    # 각 threshold에 대해서 f1-score 구하기
    threshold_matrix['f1_score'] = threshold_matrix['Cut-off'].apply(lambda x: f1_score(label_data, (prob_data>x)*1))
    
    # 최적 f1-score에 해당하는 cut-off 구하기
    optim_threshold = threshold_matrix[threshold_matrix.f1_score == threshold_matrix.f1_score.max()]['Cut-off'].min()
    
    precision = sc(**{'y_true':label_data, 'y_pred' : prob_data, 'metric':'precision', 'threshold':optim_threshold})
    recall = sc(**{'y_true':label_data, 'y_pred' : prob_data, 'metric':'recall','threshold':optim_threshold})
    accuracy = sc(**{'y_true':label_data, 'y_pred' : prob_data, 'metric':'acc','threshold':optim_threshold})
    #auc = sc(**{'y_true':label_data, 'y_pred' : prob_data, 'metric':'auc'})
    #ks = sc(**{'y_true':label_data, 'y_pred' : prob_data, 'metric':'ks'})
    
    optim_th = {
        'threshold':optim_threshold,
        'precision':precision,
        'recall':recall,
        'accuracy':accuracy,
        'f1_score':threshold_matrix[threshold_matrix['Cut-off'] == optim_threshold].f1_score.iloc[0]
        #'auc' : auc,
        #'ks' : ks
    }
    
    return optim_th

def cut_off_chart(data, label, q_range = np.arange(0.01, 0.99, 0.01)):
    """
    Args:
        data: (pd.DataFrame)
        label: (str)
    Returns:
        cut_off_matrix: (pd.DataFrame) cut-off에 따른 성능지표 결과
    """
    cut_off_matrix = pd.DataFrame(q_range, columns = ['Quantile'])
    prob_data = data[f'{label}_PROB']
    target_data = data[label]
    
    tqdm.pandas(desc='CutOff')
    cut_off_matrix['CutOff'] = cut_off_matrix['Quantile'].progress_apply(lambda x: prob_data.quantile(q=x))
    
    # Precision/Recall 구하기 위한 각 종 요소들
    cut_off_matrix['TP+FP'] = cut_off_matrix['CutOff'].apply(lambda x: (prob_data>=x).sum()) # 1로 예측한 개수
    cut_off_matrix['TN+FN'] = cut_off_matrix['CutOff'].apply(lambda x: (prob_data<x).sum()) # 0으로 예측한 개수
    cut_off_matrix['FP'] = cut_off_matrix['CutOff'].apply(lambda x: (((prob_data>=x) - target_data)>0).sum()) # 1로 예측했으나, 틀린 값의 개수
    cut_off_matrix['TP'] = cut_off_matrix['TP+FP'] - cut_off_matrix['FP']
    cut_off_matrix['FN'] = cut_off_matrix['CutOff'].apply(lambda x : abs((((prob_data>=x) - target_data)<0).sum()))
    cut_off_matrix['TN'] = cut_off_matrix['TN+FN'] - cut_off_matrix['FN']
    
    cut_off_matrix['Precision'] = cut_off_matrix['TP']/cut_off_matrix['TP+FP']
    cut_off_matrix['Recall'] = cut_off_matrix['TP']/(cut_off_matrix['TP']+cut_off_matrix['FN'])
    cut_off_matrix['F1_score'] = cut_off_matrix['Precision']*cut_off_matrix['Recall']*2/(cut_off_matrix['Precision']+cut_off_matrix['Recall'])
    cut_off_matrix['Acc'] = (cut_off_matrix['TP'] + cut_off_matrix['TN']) / len(prob_data)
    
    tqdm.pandas(desc='KS') 
    cut_off_matrix['KS'] = cut_off_matrix['CutOff'].progress_apply(lambda x : specific_ks_stat(target_data, prob_data, x))
    cut_off_matrix['Max_KS'] = cut_off_matrix['KS'].apply(lambda x : 'max' if x == cut_off_matrix.KS.max() else '')
    
    return cut_off_matrix